Je vais créer un site , pour référencer les personnages et armes du jeu Gesnhin impact
## Créer un base de données `Genshin Impact`
    -> avec une table `personnages`
        - ID 
        - Nom
        - Element
        - Région (num_region)
        - type d'armes
        - nombre d'étoiles
  
    -> avec une table `armes`
        - ID
        - Nom 
        - type d'armes 
        - nombre d'étoiles 
        - stats élévation
  
    -> avec une table `Région`
        - num_region
        - Nom de la région 
  
## Créer un utilisateur `Voyageur`
    Lui donner les droits pour la BDD `Genshin Impact`

## Créer un `header`et un `footer`
    à intégrer à toutes les pages 

## Créer une page d'accueil 
    -> une description du site et du jeu 
    -> un menu pour rediriger vers les différentes pages du sites

## Page 1 : Créer un formulaire pour insérer dans la table `personnages` les personnages 
            - input => Nom
            - input => Element
            - input => Région 
            - input => type d'armes
            - input => nombre d'étoiles

## Page 2 : Créer un formulaire pour insérer dans la table `armes` les armes 
            - input => Nom
            - input => types d'armes
            - input => nombre d'etoiles 
            - input => stat élévation

## Page 3 : Afficher les personnages existants en BDD 
        Afficher les personnages => et chaques personnages aura les information de la bdd ainsi qu'une description 

## Page 4 : Afficher les armes existantes en BDD
        Afficher les armes => et chaques armes aura les information de la bdd ainsi qu'une description 
        
