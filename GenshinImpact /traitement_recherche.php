<?php
    session_start();

    $count = $requetePrep->rowCount();
    // Si le resultat de la requête est superieur à 1 
    if($requetePrep->rowCount() > 0){

        // On affiche le nombre de résultat 
       ?> <p class="resultat">Il y a <?= $count ?> résultats</p> <?   
        // on boucle pour afficher les résultats de la recherche 
       while($data = $requetePrep->fetch()){
        ?>
        
        <article class="liste-perso">
            <button id="nom-perso"><?= $data['nom'] . NL ?></button><br/>
                    <?php
                    echo ("<img class=image-perso src=" . $data['photo'] . "/>");
                    ?>
                <div id="info">
                    <p class="d" ><?= '<span class="a">Element:</span> ' . $data['element'] . NL ?></p>
                    <p class="e" ><?= '<span class="b">Région:</span> ' . $data['region'] . NL ?></p>
                    <p class="d" ><?= '<span class="a">Arme:</span> ' . $data['armes'] . NL ?></p>
                    <p class="e" ><?= '<span class="b">Nombre d\'étoiles:</span> ' . $data['etoiles']?></p>  
                </div>
                <p class="description"><?= $data['description'] . NL ?></p>

                <h2>Armes disponibles pour le personnage</h2>
        <?php 
         // On affiche les armes qui corresponde aux type d'armes du personnages
         $requete2 ="SELECT * FROM armes WHERE type = '$data[armes]' ORDER BY etoiles DESC ";
         $requetePrep2 = $pdo->prepare($requete2);
         $requetePrep2->bindParam('nom', $nom);
         $requetePrep2->execute();

         // on affiche tous les resutlats correspondant
        while($data1 = $requetePrep2->fetch(PDO::FETCH_ASSOC)) {
            echo('<ol class="liste-armes"><li><img class="img-armes" src="' . $data1['photo'].'"/></li> '. NL);
            echo('<li> '. $data1['nom'] .'</li> - '. NL);
            echo('<li>Elevation: ' . $data1['elevation'].'</li> - '. NL);
            echo('<li>Etoiles: ' . $data1['etoiles']. '</li>'. NL);
            echo('</ol>');
            
        }   
        ?>   
        </article>
        <?

       } 
     
    }
   
    else{
        ?>
        <article class="liste-perso">
            <p class="no-result">Le personnage n'a pas été trouver</p>
            <img class ="albedo" src="asset/albedo.png" alt="Albedo">
            <a class="no-result" href="personnages.php">Enregistrez le personnage ici !!!</a>
        </article>
        <?php
    }
    ?>