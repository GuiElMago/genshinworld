<?php

define('NL', '<br/>');

$nom = $_POST['nom'];
$type = $_POST['type'];
$etoile = $_POST['nbre-etoile'];
$elevation = $_POST['elevation'];

include('pdo.php');

$requete1 = "SELECT * FROM armes WHERE nom = :nom";

$requete1Prep = $pdo->prepare($requete1);
$requete1Prep->bindParam(':nom', $nom);
$requete1Prep->execute();

$data = $requete1Prep->fetch();


if($_POST['nom'] != $data['nom']){

$requete = "INSERT INTO armes (nom, id_type, etoiles, elevation) VALUES (:nom, :type, :etoiles, :elevation)";

$requetePrep = $pdo->prepare($requete);
$requetePrep->bindParam(':nom', $nom);
$requetePrep->bindParam(':type', $type);
$requetePrep->bindParam(':etoiles', $etoile);
$requetePrep->bindParam(':elevation', $elevation);
$requetePrep->execute();

echo "<script type='text/javascript'>document.location.replace('ok-armes.php');</script>";
}

else{
    echo "<script type='text/javascript'>document.location.replace('erreur-armes.php');</script>";
}
?>


