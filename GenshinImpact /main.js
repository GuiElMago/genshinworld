
// je récupère les éléments avec la class paimon
const paimon = document.querySelector('#lien');
const paimon1 = document.querySelector('#lien1');
const paimon2 = document.querySelector('#lien2');
const paimon3 = document.querySelector('#lien3');

// quand on passe la souris sur le li , la fonction showListe s'exécute
paimon.addEventListener('mousemove', function(){
    showListe('#paimon')
});
paimon1.addEventListener('mousemove', function(){
    showListe('#paimon1')
});
paimon2.addEventListener('mousemove', function(){
    showListe('#paimon2')
});
paimon3.addEventListener('mousemove', function(){
    showListe('#paimon3')
});

// On déclare la fonction
function showListe(listeClass){
    const liste = document.querySelector(listeClass);
    if(liste.classList.contains('active')){
        liste.classList.remove('active');
    }

    else{
        for(let perso of document.querySelectorAll('.paimon')){
            perso.classList.remove('active');
        }
    }

document.querySelector(listeClass).classList.add('active');
    
}


// Affichage lettre par lettre 

const htmlP = document.getElementById("monde");
const txt = htmlP.dataset.label;
let i 	= 0 ;
function showLetters()
{
  let timeOut ;
  if(i < txt.length)
	{
	  htmlP.innerHTML += `<span>${txt[i]}</span>` ;
	  timeOut = setTimeout(showLetters,200)
	  i++
	}
	else
	{
	  clearTimeout(timeOut);
	  console.log("end")
	}
}
showLetters();