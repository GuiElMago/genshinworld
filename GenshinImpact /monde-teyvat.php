<?php
session_start();
define('NL','<br/>');


//Barre de recherche
$search = $_GET['search'] . "%"; //on stock dans une variable la valeur de rentré dans l'input search dans l'URL

include('pdo.php'); //PDO
$requete = "SELECT * FROM personnages ORDER BY nom DESC"; // On recupère les paramètres ordonner par nom 

$nbResult = 0;

// Si la variable de GET est declarée et la recherche est remplie 
if(isset($_GET['search']) && !empty($_GET['search'])){
   
    // on exécute la requete 
    $requete = 'SELECT personnages.id AS perso_id, personnages.nom, personnages.etoiles, personnages.photo, 
                        description,armes.photo AS photo_arme, regions.nom AS region, 
                        elements.nom AS element, type_armes.nom AS armes, armes.nom AS arme_favoris, 
                        favoris.id_personnages AS perso_favoris, 
                        favoris.id_users AS user_favoris 
        FROM personnages 
            JOIN regions ON id_region = regions.id 
                JOIN elements ON id_element = elements.id 
                    JOIN type_armes ON id_type = type_armes.id
                        LEFT JOIN favoris ON personnages.id = favoris.id_personnages  
                            LEFT JOIN armes ON id_armes = armes.id
        WHERE personnages.nom LIKE :nom ';

    $requetePrep = $pdo->prepare($requete);
    $requetePrep->bindParam(':nom', $search);
    //$requetePrep->bindParam(':id_user', $_SESSION['id']);
    
    $requetePrep->execute();
    // ici mettre le nb de résultats
    $nbResult = $requetePrep->rowCount();
}

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Monde de Teyvat</title>
        <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
        <link rel="stylesheet" href="main.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/06e531c103.js" crossorigin="anonymous"></script>
        <script src="main.js" defer></script>
        <script src="personnages.js" defer></script>
        
    </head>
    <body>
        <?php include('header.php');?>
        
        <h1 id="monde" data-label="Le Monde de Teyvat"></h1>
        <p class="intro1">Rechercher les personnages qui ont déjà rejoint le monde de Teyvat pour afficher leurs informations !!!</p>
        
        <section class="formulaire">
            <form class="form-s" action="" method="GET">
                <input type="search" name="search" class="search" placeholder="Rechercher le personnages"/>
                <input type="submit" value="Rechercher" class="rechercher" name="Envoyer"/>
            </form>
        </section>
        
        <section>
            <?php
        
        // Si le resultat de la requête est superieur à 1 
        if($nbResult > 0){
            
            // On affiche le nombre de résultat 
            ?> <p class="resultat">Il y a <?= $nbResult ?> résultats</p> <?   
            // on boucle pour afficher les résultats de la recherche 
            while($data = $requetePrep->fetch(PDO::FETCH_ASSOC)){
                //print_r($data);
                ?>
        
        <article class="liste-perso">
            <button id="nom-perso"><?= $data['nom'] . NL ?></button><br/>
            
            <form class="favoris1" action="#" method="POST">
            <?php

            // FAVORIS 
            //Ajout aux favoris
            $fav = <<<HTML
            <button type="submit" name="favoris" id="favoris"><i class="fas fa-heart"></i></button>
            <label for="favoris">Ajouter aux favoris</label>
            </form>
            HTML; 
            // retirer des favoris
            $remove_fav= <<<HTML
            <button type="submit" name="remove_favoris" id="remove_favoris"><i class="fas fa-heart"></i></button>
            <label for="remove-favoris">Retirer des favoris</label>
            </form>
            HTML;
            
            if(isset($data['perso_favoris'])){ //&& isset($data['perso_favoris']) && $data['perso_favoris'] == $_SESSION['id']) {
                echo $remove_fav;
            } else {
                echo $fav;
            }

            ?>

            <?php
            if(isset($_POST['favoris'])){
                    $requete1 = "INSERT INTO favoris (id_users, id_personnages) VALUES(:id_users, :id_personnages)";
                    $requetePrep1 = $pdo->prepare($requete1);
                    $requetePrep1->bindParam(':id_users', $_SESSION['id']);
                    $requetePrep1->bindParam(':id_personnages', $data['perso_id']);
                    $requetePrep1->execute(); 
                }
                
             if(isset($_POST['remove_favoris'])){
                    $requete3 = "DELETE FROM favoris WHERE id_personnages = :id";
                    $requetePrep3 = $pdo->prepare($requete3);
                    $requetePrep3->bindParam(':id',$data['perso_id']);
                    $requetePrep3->execute();
                }
                ?>
    
                    <?php
                
                    echo ("<img class=image-perso src=" . $data['photo'] . "/>");
                    ?>
                        
                <div id="info">
                    <p class="d" ><?= '<span class="a">Element:</span> ' . $data['element'] . NL ?></p>
                    <p class="e" ><?= '<span class="b">Région:</span> ' . $data['region'] . NL ?></p>
                    <p class="e" ><?= '<span class="b">Nombre d\'étoiles:</span> ' . $data['etoiles']?></p>
                    <p class="d" ><?= '<span class="a">Arme:</span> ' . $data['armes'] . NL ?></p>
                    
                    
                    <div id="arme-perso">
                    <p class="nom-armes"><?= $data['arme_favoris']?></p>
                    <img class="img-armesFav" src= "<?= $data['photo_arme']?>"/>
                </div>  
                </div>

                <p class="description"><?= $data['description'] . NL ?></p>
                
                <h2>Autres armes disponibles pour le personnage</h2>
                
        <?php 
         // On affiche les armes qui corresponde aux type d'armes du personnages
         $requete2 ="SELECT armes.nom, etoiles, elevation, photo, type_armes.nom AS armes FROM armes  JOIN type_armes ON id_type = type_armes.id WHERE  type_armes.nom = '$data[armes]' ORDER BY etoiles DESC ";
         $requetePrep2 = $pdo->prepare($requete2);
         $requetePrep2->bindParam('nom', $nom);
         $requetePrep2->execute();

         // on affiche tous les resutlats correspondant
        while($data1 = $requetePrep2->fetch(PDO::FETCH_ASSOC)) {
            //print_r($data1);
            echo('<ol class="liste-armes">
            <li><img class="img-armes" src="' . $data1['photo'].'"/></li> '. NL);
            echo('<li class="nom_armes"> '. htmlspecialchars($data1['nom']) .'</li>'. NL);
            echo('<li class="elevation">Elevation: ' . $data1['elevation'].'</li> '. NL);
            echo('<li class="etoile">Etoiles: ' . $data1['etoiles']. '</li>'. NL);
            echo'</ol>';
            
        }   
        ?>   
        
        </article>
        <?

       } 
    }
    // si il n'y a pas de recherche 
    elseif($_GET['search'] == false){
        echo '<img class="monde" src="https://www.pockettactics.com/wp-content/uploads/2021/06/genshin-impact-gorou-1.jpg" alt="Inazuma"';  
    }

    else{
        ?>
         <article class="liste-perso">
            <p class="no-result">Le personnage n'a pas été trouver</p>
            <img class ="albedo" src="asset/albedo.png" alt="Albedo">
            <a class="no-result" href="personnages.php">Enregistrez le personnage ici !!!</a>
        </article>
        <?php
    }
    
    ?>
    
</section>
<?php include('footer.html'); ?>   
</body>
</html>
