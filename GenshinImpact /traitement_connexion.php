<?php
session_start();

include('pdo.php'); // Connexion a la BDD

$pseudo = $_POST['pseudo'];
$password = $_POST['password'];

$requete = 'SELECT * FROM users WHERE pseudo = :pseudo';

$requetePrep = $pdo->prepare($requete);

$requetePrep->bindParam(':pseudo', $pseudo);
$requetePrep->execute();

$data = $requetePrep->fetch(PDO::FETCH_ASSOC);

// Si le pseudo existe et que le mot de passe correspond au mot de passe hashé en BDD
if(isset($data['pseudo']) && password_verify($password, $data['password'])){
    $_SESSION['pseudo'] = $data['pseudo'];
    $_SESSION['id'] = $data['id'];
    
    // On renvoi à la page d'accueil avec le nom de l'utilisateur donné en method GET
    header('location:accueil.php?pseudo='.$data['pseudo']);
}

else{
    header('location:inscription.php');
}



