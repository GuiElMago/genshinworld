CREATE DATABASE GenshinImpact;

CREATE TABLE personnages (id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, nom VARCHAR(80), element VARCHAR(80), num_region INT(11), armes VARCHAR(80), nbre_etoiles INT(11));

CREATE TABLE armes (id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, nom VARCHAR(80), type VARCHAR(80), nbre_etoile INT(11), elevation VARCHAR(80));

CREATE TABLE region (num_region INT(11), nom VARCHAR(80));

CREATE USER 'voyageur'@'localhost' IDENTIFIED BY 'teyvat';

GRANT ALL ON GenshinImpact.* TO 'voyageur'@'localhost' IDENTIFIED BY 'teyvat';

 INSERT INTO region (num_region, nom) VALUES('3','Inazuma');

 CREATE TABLE Favoris (
     id_favoris INT PRIMARY KEY AUTO_INCREMENT,
     num_personnage INT(11) NOT NULL,
        CONSTRAINT fk_num_personnage_nom  /* on donne le nom de la clé etrangère */
        FOREIGN KEY (num_personnage) REFERENCES personnages(id));


UPDATE personnages SET description = "Astrologue d\'une grande habileté et d\'une fierté tout aussi grande, Mona a élu domicile à Mondstadt pour éviter d\'encourir les foudres de son maître après avoir lu sans le vouloir le journal de ce dernier.' WHERE nom = 'Mona";

ALTER TABLE images ADD CONSTRAINT fk_perso_id FOREIGN KEY (perso_id) REFERENCES personnages(id);

"Version 2 Base de donnée"  

CREATE DATABASE GenshinImpactV2;

GRANT ALL ON GenshinImpactV2.* TO 'voyageur'@'localhost' IDENTIFIED BY 'teyvat';

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    pseudo VARCHAR(80), 
    password VARCHAR(200)
    );

CREATE TABLE personnages (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nom VARCHAR(80),
    etoiles INT(11),
    description TEXT,
    photo VARCHAR(200),
    id_element INT,
    id_region INT,
    id_armes INT,
    id_type INT,
    CONSTRAINT fk_element_id FOREIGN KEY (id_element) REFERENCES element(id),
    CONSTRAINT fk_region_id FOREIGN KEY (id_region) REFERENCES region(id),
    CONSTRAINT fk_armes_id FOREIGN KEY (id_armes) REFERENCES armes(id),
    CONSTRAINT fk_type_id FOREIGN KEY (id_type) REFERENCES type_armes(id)
);

CREATE TABLE favoris (
    id_users INT,
    id_personnages INT,
    PRIMARY KEY(id_users, id_personnages),
    CONSTRAINT fk_users_id FOREIGN KEY (id_users) REFERENCES users(id),
    CONSTRAINT fk_personnages_id FOREIGN KEY (id_personnages) REFERENCES personnages(id)
);

CREATE TABLE type_armes (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(80)
);

CREATE TABLE armes (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(80),
    id_type INT,
    etoiles INT,
    elevation VARCHAR(100),
    photo VARCHAR(200)

);

CREATE TABLE regions (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(80)  
);

CREATE TABLE elements (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(80)
);

INSERT INTO regions (nom) VALUES('Mondstadt');

INSERT INTO elements (nom) VALUES('Arc');

UPDATE armes SET photo = "https://uploadstatic.mihoyo.com/ys-obc/2020/07/17/75795471/cda9841711c3d126c81a531a02e94953_3658752798229410879.png"  WHERE id='9';

UPDATE personnages SET photo = "https://ih1.redbubble.net/image.1726009597.2509/fposter,small,wall_texture,product,750x1000.jpg"  WHERE id='8';

UPDATE personnages SET description = "En tant que seule Outrider restante des Chevaliers de Favonius , elle est toujours prête à aider les citoyens de Mondstadt , que ce soit quelque chose de simple ou peut-être une tâche plus difficil"  WHERE id='8';



