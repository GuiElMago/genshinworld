<?php
session_start();
  //$_SESSION['pseudo'] = $_GET['pseudo'];
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil-GenshinWorld</title>
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
    <link rel="stylesheet" href="main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <script src="main.js" defer></script>
</head>
<body>
    <?php include('header.php');?>

    <h1 id="monde" data-label="Bienvenue <?php echo htmlspecialchars($_SESSION['pseudo']); ?> sur GenshinWorld"></h1>
        
        <p class="intro">Un site fait pour les passionner de genshin impact et son univers !!!</p>
    <section id="presentation">
        <article class="p1">
            <h2 class="titre-p">Un univers riches en personnages, en histoires dans différentes régions</h2>
            <p class="p">Le Monde de Teyvat est en constante évolution avec la découverte de nouveaux personnages, de nouvelles régions ainsi que de nouvelles histoire passionnantes, ce site est fait pour enrichir le contenu en temps réel et de façon interactive.</p>
            <img class="photo-p" src="asset/world.jpg" alt="decouverte inazuma">
        </article>
            <article class="p2">
                <h2 class="titre-p">Des armes redoutables pour tous les styles</h2>
                <p class="p">Le monde est aussi rempli d'un arsenal aussi unique que redoutable pour vous accompagnez dans vos différentes quêtes et objectif ! Vous pourrez aussi étoffer l'inventaire des armes au fil de vos aventures</p>
                <img class="photo-p" src="asset/world2.jpg" alt="combat inazuma">
        </article>
    </section>
    <?php include("footer.html") ?>
</body>
</html>