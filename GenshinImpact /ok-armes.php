<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ok-Armes</title>
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/06e531c103.js" crossorigin="anonymous"></script>
    <script src="main.js" defer></script>
</head>
<body>
    <?php include('header.php');?>
    <div id="redirection">
        <p class="intro1" id="ok">Votre arme a bien rejoint votre arsenal !!!</p>
        <img class="tarta" src="asset/tarta.png" alt="Tartaglia">  
    </div>
   
    <?php include('footer.html');?>
</body>
</html>