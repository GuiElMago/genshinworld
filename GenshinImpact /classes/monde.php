<?php
session_start();
define('NL','<br/>');


//Barre de recherche
$search = $_GET['search'] . "%"; //on stock dans une variable la valeur de rentré dans l'input search dans l'URL

include('../pdo.php'); //PDO
$requete = "SELECT * FROM personnages ORDER BY nom DESC"; // On recupère les paramètres ordonner par nom 

$nbResult = 0;

// Si la variable de GET est declarée et la recherche est remplie 
if(isset($_GET['search']) && !empty($_GET['search'])){
    
    // on exécute la requete 
    $requete = 'SELECT personnages.id AS perso_id, personnages.nom, personnages.etoiles, personnages.photo, 
    description,armes.photo AS photo_arme, regions.nom AS region, 
    elements.nom AS element, type_armes.nom AS armes, armes.nom AS arme_favoris, 
    favoris.id_personnages AS perso_favoris, 
    favoris.id_users AS user_favoris 
    FROM personnages 
    JOIN regions ON id_region = regions.id 
    JOIN elements ON id_element = elements.id 
    JOIN type_armes ON id_type = type_armes.id
    LEFT JOIN favoris ON personnages.id = favoris.id_personnages  
    LEFT JOIN armes ON id_armes = armes.id
    WHERE personnages.nom LIKE :nom ';
    
    $requetePrep = $pdo->prepare($requete);
    $requetePrep->bindParam(':nom', $search);
    //$requetePrep->bindParam(':id_user', $_SESSION['id']);
    
    $requetePrep->execute();
    // ici mettre le nb de résultats
    $nbResult = $requetePrep->rowCount();
}             

// Si le resultat de la requête est superieur à 1 
if($nbResult > 0){
    
    // On affiche le nombre de résultat 
    // on boucle pour afficher les résultats de la recherche 
    while($data = $requetePrep->fetch(PDO::FETCH_ASSOC)){
        //print_r($data);
        ?>
        
        <?php
        
        // FAVORIS 
        //Ajout aux favoris
        
        
        ?>
        
        <?php
        if(isset($_POST['favoris'])){
            $requete1 = "INSERT INTO favoris (id_users, id_personnages) VALUES(:id_users, :id_personnages)";
            $requetePrep1 = $pdo->prepare($requete1);
            $requetePrep1->bindParam(':id_users', $_SESSION['id']);
            $requetePrep1->bindParam(':id_personnages', $data['perso_id']);
            $requetePrep1->execute(); 
        }
        
        if(isset($_POST['remove_favoris'])){
            $requete3 = "DELETE FROM favoris WHERE id_personnages = :id";
            $requetePrep3 = $pdo->prepare($requete3);
            $requetePrep3->bindParam(':id',$data['perso_id']);
            $requetePrep3->execute();
        }
        ?>
        
        <?php
        
        
        
        // On affiche les armes qui corresponde aux type d'armes du personnages
        $requete2 ="SELECT armes.nom, etoiles, elevation, photo, type_armes.nom AS armes FROM armes  JOIN type_armes ON id_type = type_armes.id WHERE  type_armes.nom = '$data[armes]' ORDER BY etoiles DESC ";
        $requetePrep2 = $pdo->prepare($requete2);
        $requetePrep2->bindParam('nom', $nom);
        $requetePrep2->execute();
        
        // on affiche tous les resutlats correspondant
        while($data1 = $requetePrep2->fetch(PDO::FETCH_ASSOC)) {
            //print_r($data1);
            
        }

        echo"{}";
        
        
    } 
}
   