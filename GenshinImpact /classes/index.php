<?php
session_start();
$search = $_GET['search']."%";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/06e531c103.js" crossorigin="anonymous"></script>
    <script src="../main.js" defer></script>
    <script src="./favoris.js" defer></script>
    <title>Document</title>
    <link rel="stylesheet" href="../main.css">
    <script src="./test.js" defer></script>
</head>
<body>
<section class="formulaire">
            <form id="form-s" class="form-s" action="" method="POST">
                <input type="search" name="search" class="search" placeholder="Rechercher le personnages"/>
                <input type="submit" value="Rechercher" class="rechercher" name="Envoyer"/>
            </form>
        </section>
    
    <!--<button id="button">touch</button>-->
    
</body>
</html>