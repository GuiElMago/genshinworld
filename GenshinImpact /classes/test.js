document.getElementById('form-s')
.addEventListener('submit', (ev)=>{
    ev.preventDefault()
    createElement()
    console.log('test')
})
    function createElement() {
    // Envoie le résultat de de l'objet json_encode de la page TEST.php 
    fetch('./TEST.php')
    .then(result => result.json())
    .then(players => {

        let i = 0
        for(let player of players) {
            console.log(player)
            i++
            const article = document.createElement('article')
            article.className = 'liste-perso'
            document.querySelector('body').appendChild(article)
            
            const nom = document.createElement('h2')
            nom.innerText = player.nom
            nom.id = 'nom-perso'
            article.appendChild(nom)
            
            const image = document.createElement('img')
            image.src = player.photo
            image.className = 'image-perso'
            article.appendChild(image)
            
            
            const info = document.createElement('div')
            info.id = 'info'
            article.appendChild(info)
            
            // Favoris
            const bouton = document.createElement('button')
            bouton.id = 'favoris-'+ i
            bouton.className = 'favoris'
            info.appendChild(bouton)
            const favoris = document.createElement('i')
            favoris.className ='fas fa-heart'
            bouton.appendChild(favoris)
            const ajout = document.createElement('figcaption')
            ajout.className = 'favoris_figcaption'
            ajout.innerText = 'Ajouter aux favoris'
            favoris.appendChild(ajout)
            
            const element = document.createElement('p')
            element.className = 'a'
            element.innerText = 'Element : '+ player.element
            info.appendChild(element)
            
            const etoiles = document.createElement('p')
            etoiles.className = 'a'
            etoiles.innerText = 'Etoiles: '+ player.etoiles
            info.appendChild(etoiles)
            
            const region = document.createElement('p')
            region.className = 'a'
            region.innerText = 'Région : ' + player.region
            info.appendChild(region)
            
            const arme = document.createElement('p')
            arme.className = 'b'
            arme.innerText = player.arme.nom
            info.appendChild(arme)
            
            const imageArme = document.createElement('img')
            imageArme.src = player.arme.photo
            imageArme.className ='img-armes1'
            info.appendChild(imageArme)
            
            const description = document.createElement('p')
            description.className = 'description'
            description.innerText = player.description
            article.appendChild(description)
            
            const titreAutres = document.createElement('h2')
            titreAutres.innerText = 'Autres armes disponibles pour le personnages'
            article.appendChild(titreAutres)
            
            for(let armeSecond of player.autresArmes) {
                
                const armesType = document.createElement('div')
                armesType.className = 'liste-secondArmes'
                article.appendChild(armesType)
                
                const photoArmes = document.createElement('img')
                const diversArmes = document.createElement('p')
                const elevation = document.createElement('p')
                const armesEtoiles = document.createElement('p')
                
                
                diversArmes.innerText = armeSecond.nom
                diversArmes.className = 'second-armes'
                photoArmes.src = armeSecond.photo
                photoArmes.className = 'imgSecond_armes'
                elevation.innerText = armeSecond.elevation
                elevation.className = 'second-armes'
                armesEtoiles.innerText = armeSecond.etoiles
                armesEtoiles.className = 'second-armes'
                
                armesType.appendChild(photoArmes)
                armesType.appendChild(diversArmes)
                armesType.appendChild(elevation)
                armesType.appendChild(armesEtoiles)
                
            }
            
        }
        
        
    })
    
}