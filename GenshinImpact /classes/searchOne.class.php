<?php
// On créer une classe "searchOne" avec ses paramètres en public 
class Personnage {
    public $perso_id;
    public $nom;
    public $etoiles;
    public $photo;
    public $description;
    public $arme;
    public $favoris;
    public $region;
    public $autresArmes = [];
    public $element;
    
    // On déclare une méthode "constructeur" qui va initialisé un objet avec les propriétés de la classe avec les paramètres donnée  
    public function __construct($user_id, $perso_id)
    {   
        //Connexion PDO
        include('../pdo.php');

        //Requête qui récupère les paramètres d'un personnages en bdd 
        $requete = 'SELECT personnages.id AS perso_id, personnages.nom AS nom, personnages.etoiles AS etoiles, personnages.photo AS   photo, description, regions.nom AS region, elements.nom AS element
        FROM personnages 
        JOIN regions ON id_region = regions.id 
        JOIN elements ON id_element = elements.id 
        WHERE personnages.id = :perso_id';
        
        // On bind les paramètres 
        $stmt = $pdo->prepare($requete);
        $stmt->bindParam(':perso_id', $perso_id);
        $stmt->execute();
        
        // On parcours les résultat de la requête 
        while ($row = $stmt->fetch()) {
            // On pointe les propriétés de la classe pour leur donner la valeur du résultat de la requête 
            $this->perso_id = $row['perso_id'];
            $this->nom = $row['nom'];
            $this->etoiles = $row['etoiles'];
            $this->photo = $row['photo'];
            $this->description = $row['description'];
            $this->region = $row['region'];
            $this->element = $row['element'];
        }
        
        // On appel les méthodes 
        $this->favoris($pdo, $user_id, $perso_id);
        $this->armes($pdo, $perso_id);
        
        // On récupère les propriétés type et nom de la méthode armes , dans des variables
        $type = $this->arme['type'];
        $nomArmes = $this->arme['nom'];
        // Qui sont en paramètres de la méthode autresArmes
        $this->autresArmes($type, $nomArmes, $pdo);
        
        
    }
    
    // On déclare une méthode "favoris" qui à pour paramètres l'user_id , le perso_id et le pdo 
    private function favoris($pdo, $user_id, $perso_id) {
        $req = 'SELECT * 
        FROM favoris 
        WHERE id_users = :users_id
        AND id_personnages = :perso_id';
        
        $stmt = $pdo->prepare($req);
        $stmt->bindParam(':users_id', $user_id);
        $stmt->bindParam(':perso_id', $perso_id);
        $stmt->execute();
        $result = $stmt->fetch();
        
        //Si il y a un résultat la propriété favoris est true 
        if($result['id_personnages']) {
            $this->favoris = true;
        }
        // Sinon elle est false 
        else {
            $this->favoris = false;
        }
    }
    
    //On déclare une méthode "armes" qui à pour paramètres le perso_id et le pdo 
    private function armes ($pdo, $perso_id) { 
        $sql = "SELECT armes.nom, armes.photo, armes.etoiles, armes.elevation, type_armes.nom AS type FROM armes
        JOIN type_armes ON armes.id_type = type_armes.id
        WHERE armes.id = (
            SELECT personnages.id_armes FROM personnages WHERE personnages.id = :perso_id
            )";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':perso_id', $perso_id);
            $stmt->execute();
            
            // On parcours les résultat et on stock le résultat dans une variable row 
            while($row = $stmt->fetch()) {
                // la propriété arme de la classe Personnages 
                $this->arme = [
                    'nom' => $row['nom'],
                    'photo' => $row['photo'],
                    'type' => $row['type'],
                    'etoiles' => $row['etoiles'],
                    'elevation' => $row['elevation']
                ];
            }
        }
        
        // On déclare une méthode "autresArmes" qui à pour paramètres type, nom et pdo 
        private function autresArmes($type, $nom, $pdo){
            $sql = "SELECT armes.nom, armes.photo, armes.etoiles, armes.elevation, type_armes.nom AS type FROM armes
            JOIN type_armes ON armes.id_type = type_armes.id
            WHERE type_armes.nom = :type
            AND armes.nom != :nom" ;
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':type', $type);
            $stmt->bindParam(':nom', $nom);
            $stmt->execute();
            
            while($row = $stmt->fetch()) {
                // la propriété autresArmes de la classe Personnage
                $this->autresArmes[] = [
                    'nom' => $row['nom'],
                    'photo' => $row['photo'],
                    'type' => $row['type'],
                    'etoiles' => $row['etoiles'],
                    'elevation' => $row['elevation']
                ];
            }
        }
        
    }

