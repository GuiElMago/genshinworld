<?php

header('Content-Type: application/json');
session_start();
$search = $_GET["search"] . "%";
$userId = 1;
$result = [];

include('./searchOne.class.php');
include('../pdo.php');
$req = "SELECT id FROM personnages WHERE nom LIKE :search";
$stmt = $pdo->prepare($req);
$stmt->bindParam(':search', $search);
$stmt->execute();
while($row = $stmt->fetch()) {
    $result[] = $row['id'];
}

$return = [];
foreach($result as $perso_id) {
    // On instancie un nouvel objet Personnage avec pour paramètre l'user_id et le perso_id
    $search = new Personnage($user_id, $perso_id);
    $return[] = $search;
}


echo json_encode($return);

