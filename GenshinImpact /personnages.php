<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personnages-GenshinWorld</title>
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/06e531c103.js" crossorigin="anonymous"></script>
    <script src="main.js" defer></script>
   <!-- <script src="region.js" defer></script> -->
</head>
<body>
    <?php include('header.php');?>
    <h1 id="monde" data-label="Ajouter des Personnages"></h1>
        <p class="intro">Vous avez une formulaire à disposition pour rajouter des personnages de l'univers de Genshin impact et rajouter quelques informations, allez y !!!</p>
    <section>

        <form id="form-perso" action="traitement-personnages.php" method="POST">
            <label for="nom">Nom</label><input type="text" name="nom" id="nom" required>
            <p class="title-radio">Elément:</p>
            <div id="liste-element">  
                    <input class="radio-input" type="radio" value="1" id="anemo" name="element" checked>
                    <label for="anemo"><img class="png1" src="asset/anemo.png" alt="anemo"/><figcaption>Anémo</figcaption></label>
                    <input class="radio-input" type="radio" value="2" id="pyro" name="element">
                    <label for="pyro"><img class="png1" src="asset/pyro.png" alt="pyro"/><figcaption>Pyro</figcaption></label>
                    <input class="radio-input" type="radio" value="3" id="cryo" name="element">
                     <label for="cryo"><img class="png1" src="asset/cryo.png" alt="cryo"/><figcaption>Cryo</figcaption></label>
                    <input class="radio-input" type="radio" value="4" id="geo" name="element">
                    <label for="geo"><img class="png1" src="asset/geo.png" alt="geo"/><figcaption>Géo</figcaption></label>
                    <input class="radio-input" type="radio" value="5" id="hydro" name="element">
                    <label for="hydro"><img class="png1" src="asset/hydro.png" alt="hydro"/><figcaption>Hydro</figcaption></label>
                    <input class="radio-input" type="radio" value="6" id="electro" name="element">
                    <label for="electro"><img class="png1" src="asset/electro.png" alt="electro"/><figcaption>Electro</figcaption></label>
            </div>
            <label for="region-select">Choisis une région:</label>

            <select name="region" id="region-select">
                <option value="">--Région--</option>
                <option value="1" >Mondstadt</option>
                <option value="2">Liyue</option>
                <option value="3" >Inazuma</option>
            </select>

                <label for="region-select">Choisis une arme:</label>

            <select name="armes" id="armes-select">
                <option value="">--Armes--</option>
                <option value="1">Epée à une main</option>
                <option value="2">Epée à deux main</option>
                <option value="4">Lance</option>
                <option value="5">Catalyseur</option>
                <option value="3">Arc</option>
            </select>
            
            <label for="etoile-select"><i class="fas fa-star"></i></label>

            <select name="etoiles" id="etoile-select">
                <option value="">--Etoile--</option>
                <option value='4'>4</option>
                <option value='5'>5</option>
            </select>

            <img class="sayu" src="asset/sayu2.png" alt="sayu"/>
            <input  class="bouton1" type="submit" value="Envoyer"/>
        </form>
    </section>
    <?php include("footer.html") ?>
</body>
</html>

