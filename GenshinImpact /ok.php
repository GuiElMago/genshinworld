<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OK-GenshinWorld</title>
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
    <script src="main.js" defer></script>
</head>
<body>
    <?php include('header.php'); ?>
    <div id="redirection">
    <p class="intro1" id="ok">Votre personnages est bien arriver à Teyvat !!!</p>
    <img class="zonglhi" src="asset/zonghli.png" alt="Zonghli"/>
    </div>
    <?php include('footer.html');?>
</body>
</html>