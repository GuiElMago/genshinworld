<?php 
    session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <div id=entete>
        <img id="banniere" src="asset/images-removebg-preview.png" alt="titre Genshin">
        
        <div id="itemCo">
        <div class="user">
             <img class="the" src="asset/Serenitheiere-150x150.png" alt="sérénitheiere"/>
             <figcaption><?php echo $_SESSION['pseudo'] ?></figcaption>
        </div>
        <div class="deconnexion">
            <a href="deconnexion.php"><img class="amber" src="asset/361870594039211.webp" alt="Amber"></a>
            <figcaption>Déconnexion</figcaption>

        </div>
    </div>
</div>

    <menu id="menu">
        <ul>
            <img id="paimon" class="paimon" src="asset/paimon.png" alt="paimon"><li><a id="lien" class="lien-header" href="accueil.php"><img src="asset/mini_mora.png" alt="mora"/><figcaption>Accueil</figcaption></a></li>

            <img id="paimon1" class="paimon" src="asset/paimon.png" alt="paimon"><li><a id="lien1" class="lien-header" href="personnages.php"><img src="asset/mini_mora.png" alt="mora"/><figcaption>Personnages</figcaption></a></li>

            <img id="paimon2" class="paimon" src="asset/paimon.png" alt="paimon"> <li><a id="lien2" class="lien-header" href="armes.php"><img src="asset/mini_mora.png" alt="mora"/><figcaption>Armes</figcaption></a></li>

            <img id="paimon3" class="paimon" src="asset/paimon.png" alt="paimon"><li><a id="lien3" class="lien-header" href="monde-teyvat.php"><img src="asset/mini_mora.png" alt="mora"/><figcaption>Monde de Teyvat</figcaption></a></li>
            
        </ul>
    </menu>
    
</body>
</html>