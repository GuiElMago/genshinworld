<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ok-Armes</title>
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/06e531c103.js" crossorigin="anonymous"></script>
    <script src="main.js" defer></script>
</head>
<body>
    <?php include('header.php');?>
    <div id="redirection">
        <p class="intro1" id="ok">L'arme n'est toujours pas d'étruite pas besoin de la refabriquer !!!</p>
        <img class="diona" src="asset/diona.png" alt="diona">  
    </div>
    <?php include('footer.html');?>
</body>
</html>