<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/06e531c103.js" crossorigin="anonymous"></script>
    <script src="password_inscription.js" defer></script>
    <title>Inscription-GenshinWorld</title>
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
</head>

<body>
    <img class="logoInscription" src="asset/images-removebg-preview.png" alt="logo">
    <h1>GenshinWorld</h1>
    <p class="intro1">Inscrivez vous pour pouvoir participer au voyage en Teyvat</p>

    <section class="formulaire">
    <form class="form-s" action="traitement_inscription.php" method="POST">
    <label for="pseudo">Pseudo</label><input type="text" name="pseudo" id="pseudo">
    <label for="password">Mot de passe</label><input type="password" name="password" id="password">
    <div id="passwordS"></div>
    <label for="passwordC">Comfirmez le mot de passe</label><input type="password" name="passwordC" id="passwordC">
    <div id="passwordConfirm"></div>
    <input class="bouton" type="submit" value="Valider">
    </form>
</section>
<p class="connexion">Si vous êtes déjà inscrit <a href="index.php">cliquez ici</a></p>
<?php include("footer.html") ?>
</body>
</html>