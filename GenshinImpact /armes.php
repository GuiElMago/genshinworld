<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Armes-GenshinWorld</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
    <link rel="icon" type="image/png" sizes="16x16" href="asset/Icône_Étude_des_coutumes_Brutocollinus.png"/>
    <script src="https://kit.fontawesome.com/06e531c103.js" crossorigin="anonymous"></script>
    <script src="main.js" defer></script>
</head>
<body>
<?php include('header.php');?>

<h1 id="monde" data-label="Ajouter des Armes"></h1>
        <p class="intro">Vous avez une formulaire à disposition pour ajouter des armes de l'univers de Genshin impact , allez y !!!</p>
        
<form id="form-armes" action="traitement-armes.php" method="POST">

               <img class="epee" src="asset/epee-png.png" alt="epee">
        
                    <label for="nom">Nom</label><input type="text" name="nom" id="nom" required>
              
                    <label for="region-select">Choisis une arme:</label>

                    <select name="type" id="armes-select">
                            <option value="">--Armes--</option>
                            <option value="1">Epée à une main</option>
                            <option value="2">Epée à deux main</option>
                            <option value="4">Lance</option>
                            <option value="5">Catalyseur</option>
                            <option value="3">Arc</option>
                    </select>
            
            <label for="etoile-select"><i class="fas fa-star"></i></label>

            <select name="nbre-etoile" id="etoile-select">
                <option value="">--Etoile--</option>
                <option value='3'>3</option>
                <option value='4'>4</option>
                <option value='5'>5</option>
            </select>

            <label for="elevation-select">Choisis le type d'élévation de l'armes:</label>

                <select name="elevation" id="elevation-select">
                    <option value="">--Elevation--</option>
                    <option value="degat-crit">Dégâts critiques</option>
                    <option value="taux-crit">Taux critiques</option>
                    <option value="recharge%">Recharge d'energie</option>
                    <option value="maitrise-elementaire">Maitrise élémentaires</option>
                    <option value="atk%">Attaque %</option>
                    <option value="def%">Defense %</option>
                    <option value="pv%">PV %</option>
                    <option value="dg-electro">Dégâts électro</option>
                    <option value="dg-pyro">Dégâts pyro</option>
                    <option value="dg-hydro">Dégâts hydro</option>
                    <option value="dg-cryo">Dégâts cryo</option>
                    <option value="dg-geo">Dégâts geo</option>
                    <option value="dg-anemo">Dégâts anemo</option>
                    <option value="dg-physique">Dégâts physique</option>
                </select>

            <img class="sayu" src="asset/lumine.png" alt="lumine"/>
            <input  class="bouton1" type="submit" value="Envoyer"/>
        </form>
<?php include('footer.html');?>
</body>
</html>