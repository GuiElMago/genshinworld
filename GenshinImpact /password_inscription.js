//On vérifie si les champs password et confirmation password est remplie 
function areEmptyFields(){
    if(
    document.getElementById('password').value.length <= 0 || document.getElementById('passwordC').value.length <= 0 ){
        return true
    }

    else{
        return false
    }
}
// on vérifie que le mot de passe et la confirmation est équivalent 
function passwordEgualPasswordC(){

    const password = document.getElementById('password').value
    const passwordC = document.getElementById('passwordC').value
    console.log('test')

    if(password === passwordC && !areEmptyFields()){
    document.getElementById('passwordConfirm').innerText = 'Le mot de passe est bien confirmé !'
    document.getElementById('passwordConfirm').classList.add("green")
    document.getElementById('passwordConfirm').classList.remove("red")

    document.querySelector('.bouton').style.display = 'initial'
    
    }
    else{
       document.getElementById('passwordConfirm').innerText = 'Le mot de passe n\'est pas équivalent à la confirmation'

       document.getElementById('passwordConfirm').classList.remove("green")
       document.getElementById('passwordConfirm').classList.add("red")

       document.querySelector('.bouton').style.display = 'none'
    }
}

// Si le mot de passe et sa confirmation a une longueur de plus de 5 caractères on appel la fonction passwordEgualPassword
function actionValide(){
        if(document.getElementById('password').value.length > 5 && document.getElementById('passwordC').value.length > 5){
            
            passwordEgualPasswordC()
        }

        else{
            
            document.querySelector('.bouton').style.display = 'none'
            document.getElementById('passwordConfirm').innerText = 'Il n\'y a pas assez de caractères'
            document.getElementById('passwordConfirm').classList.add("red")
        }

}

function passwordStrong(){
    const password = document.getElementById('password').value.length
    const passwordC = document.getElementById('passwordC').value.length

    if(password >= 1 && password <= 6){
        document.getElementById('passwordS').innerText = 'Mot de passe trop court'
        document.getElementById('passwordS').classList.add("red")
        document.getElementById('passwordS').classList.remove("green")
        document.getElementById('passwordS').classList.remove("orange")
        document.querySelector('.bouton1').style.display = 'none'
    }
    else if(password >= 6 && password <= 10){
        document.getElementById('passwordS').innerText = 'Mot de passe moyens'
        document.getElementById('passwordS').classList.remove("red")
        document.getElementById('passwordS').classList.remove("green")
        document.getElementById('passwordS').classList.add("orange")
        document.querySelector('.bouton1').style.display = 'initial'
    }
    else if(password >= 10 && password <= 15){
        document.getElementById('passwordS').innerText = 'Mot de passe fort'
        document.getElementById('passwordS').classList.remove("red")
        document.getElementById('passwordS').classList.remove("orange")
        document.getElementById('passwordS').classList.add("green")
        document.querySelector('.bouton1').style.display = 'initial'
    }
}


document.getElementById('password').addEventListener('keyup',passwordStrong)

    // événement apparition du message 
   const event = document.getElementById('password').addEventListener('keyup',actionValide)
   const event1 = document.getElementById('passwordC').addEventListener('keyup',actionValide)

   

    

